<?xml version="1.0" encoding="UTF-8" ?>
<!--
    ${tableComment} Mapper
    ${remark}

    author ${author}
    create ${date}
    ${brand}
-->
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="${namespace}">
    <resultMap id="BaseResultMap${simple?string('','Generator')}" type="${beanReference}">
        <#list originalFileds as filed>
            <#if filed.primaryKey>
            <id column="${filed.originalName}" jdbcType="${filed.type}" property="${filed.backCamelName}" />
            <#else>
            <result column="${filed.originalName}" jdbcType="${filed.type}" property="${filed.backCamelName}" />
            </#if>
        </#list>
    </resultMap>
    <#if simple>
    <!-- business sql start-->
    </#if>

<#if !simple>
    <insert id="insertSelective" parameterType="${beanReference}">
        insert into ${tableName}
        <trim prefix="(" suffix=")" suffixOverrides=",">
        <#list originalFileds as filed>
            <if test="${filed.backCamelName} != null">
                ${filed.escapesName},
            </if>
        </#list>
        </trim>
        <trim prefix="values (" suffix=")" suffixOverrides=",">
        <#list originalFileds as filed>
            <if test="${filed.backCamelName} != null">
                ${r"#{"}${filed.backCamelName}},
            </if>
        </#list>
        </trim>
    </insert>

    <#-- 依据主键个数生成 start -->
    <#list originalFileds as filed>
    <#if filed.primaryKey>
    <delete id="deleteBy${filed.camelName}" parameterType="string">
        delete from ${tableName} where ${filed.escapesName} = ${r"#{"}${filed.backCamelName}}
    </delete>

    <delete id="batchDeleteBy${filed.camelName}" parameterType="string">
        delete from ${tableName}
        where ${filed.escapesName} in
        <include refid="BaseBatch${simple?string('','Generator')}"></include>
    </delete>

    <update id="updateBy${filed.camelName}">
        update ${tableName}
        <include refid="BaseSet${simple?string('','Generator')}"></include>
        where ${filed.escapesName} = ${r"#{"}bean.${filed.backCamelName}}
    </update>

    <update id="batchUpdateBy${filed.camelName}">
        update ${tableName}
        <include refid="BaseSet${simple?string('','Generator')}"></include>
        where id in
        <include refid="BaseBatch${simple?string('','Generator')}"></include>
    </update>

    <select id="selectBy${filed.camelName}" resultMap="BaseResultMap${simple?string('','Generator')}"  parameterType="string">
        select * from  ${tableName}
        where ${filed.escapesName} = ${r"#{"}${filed.backCamelName}}
    </select>

        </#if>
    </#list>
    <#-- 依据主键个数生成 end -->
    <select id="selectAll" resultMap="BaseResultMap${simple?string('','Generator')}">
        select * from  ${tableName}
    </select>

    <select id="selectOne" resultMap="BaseResultMap${simple?string('','Generator')}">
        select  ${r"${"}condition.query} from ${tableName}
        <include refid="BaseWhere${simple?string('','Generator')}"></include>
    </select>

    <select id="isExist" resultType="java.lang.Boolean">
        select count(1) from ${tableName}
        <include refid="BaseWhere${simple?string('','Generator')}"></include>
    </select>

    <select id="selectCondition" resultMap="BaseResultMap${simple?string('','Generator')}">
        select
        <if test="condition.distinct">
            distinct
        </if>
        ${r"${"}condition.query} from ${tableName}
        <include refid="BaseWhere${simple?string('','Generator')}"></include>
        <if test="condition.orderBy != null">
            order by  ${r"${"}condition.orderBy}
        </if>
        <if test="condition.offset != null">
            limit ${r"${"}condition.offset},${r"${"}condition.row}
        </if>
    </select>

    <select id="selectConditionCount" resultType="java.lang.Long">
        select count(1) from ${tableName}
        <include refid="BaseWhere${simple?string('','Generator')}"></include>
        <if test="condition.orderBy != null">
            order by  ${r"${"}condition.orderBy}
        </if>
        <if test="condition.offset != null">
            limit ${r"${"}condition.offset},${r"${"}condition.row}
        </if>
    </select>
</#if>

    <!-- autoGenerated base sql start-->
    <sql id="BaseSet${simple?string('','Generator')}">
        <set>
            <#list originalFileds as filed>
            <#if !filed.primaryKey>
            <if test="bean.${filed.backCamelName} != null">
                ${filed.escapesName} = ${r"#{"}bean.${filed.backCamelName}}
            </if>
            </#if>
            </#list>
        </set>
    </sql>

    <sql id="BaseWhere${simple?string('','Generator')}">
        <where>
            <#list moreFileds as filed>
            <if test="bean.${filed.backCamelName} != null">
                <#if !filed.acquired>
                <if test="!condition.like">
                   and ${filed.escapesName} = ${r"#{"}bean.${filed.backCamelName}}
                </if>
                <if test="condition.like">
                   and ${filed.escapesName} like ${r"#{"}bean.${filed.backCamelName}}
                </if>
                <#else >
                ${filed.escapesName} = ${r"#{"}bean.${filed.backCamelName}}
                </#if>
            </if>
            </#list>
        </where>
    </sql>

    <#if !simple>
    <sql id="BaseBatch${simple?string('','Generator')}">
        <foreach close=")" collection="idList" item="id" open="(" separator=",">
            ${r"#{"}id}
        </foreach>
    </sql>
    </#if>
</mapper>