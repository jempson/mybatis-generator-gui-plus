package ${referencePackage};

<#list imports as import>
import ${import};
</#list>

/**
 * ${remark}
 *
 * @author ${author}
 * @create ${date}
 * ${brand}
 **/
<#list annotations as anno>
 ${anno}
</#list>
public interface ${daoName} extends BaseDao<${beanName},${beanName}DTO> {
    //Business code


    //Auto-generated method stub
<#-- 依据主键个数生成 start -->
<#list fileds as filed>
    /**
     *  根据${filed.originalName}主键删除记录
     *  @param ${filed.backCamelName} ${filed.backCamelName}
     */
    int deleteBy${filed.camelName}(@Param("${filed.backCamelName}") String ${filed.backCamelName});

    /**
     *  根据${filed.originalName}主键批量删除数据
     *  @param idList ${filed.backCamelName}列表
     */
    int batchDeleteBy${filed.camelName}(@Param("idList") List<String> idList);

    /**
     *  根据${filed.originalName}主键更新记录
     *  @param bean ${beanName}
     */
    int updateBy${filed.camelName}(@Param("bean") ${beanName} bean);

    /**
     *  根据${filed.originalName}主键批量更新数据
     *  仅限更新值为一样的情况
     *  @param bean ${beanName}
     *  @param idList ${filed.backCamelName}列表
     */
    int batchUpdateBy${filed.camelName}(@Param("bean") ${beanName} bean, @Param("idList") List<String> idList);

    /**
     *  根据${filed.originalName}主键查询记录
     *  @param id ${filed.backCamelName}
     */
    ${beanName} selectBy${filed.camelName}(@Param("id") String id);

</#list>
<#-- 依据主键个数生成 end -->
}
