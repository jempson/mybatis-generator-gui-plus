package test.syl;

import com.google.gson.Gson;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.syl.generator.MainUI;
import com.syl.generator.code.MybatisCodeGenerator;
import com.syl.generator.common.api.BiGenerator;
import com.syl.generator.common.api.bean.BaseBuild;
import com.syl.generator.common.bean.DatabaseBean;
import com.syl.generator.common.bean.SystemConfigBean;
import com.syl.generator.common.dao.SqlLiteMapper;
import com.syl.generator.common.enums.DbType;
import com.syl.generator.common.enums.FileType;
import com.syl.generator.common.util.ConfigReadUtil;
import com.syl.generator.common.util.DbConnectionUtil;
import com.syl.generator.view.bean.GeneratorConfig;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.syl.generator.common.constant.BaseConstant.SQLLITE_DB_ID;
import static com.syl.generator.common.constant.BaseConstant.TEMPLATE_MAPPER_DIRECTORY;

/**
 * MyTest Tester.
 *
 * @author <Authors name>
 * @since <pre>���� 23, 2018</pre>
 * @version 1.0
 */
public class MyTestTest {
    private static final Logger LOG = Logger.getLogger(MyTestTest.class);

    @Test
    public void MyTest(){

    }

    @Test
    public void generatorXML(){
        String path = "K:\\IdeaProjects\\my-crud-demo\\src\\main\\java\\com\\syl\\crud\\demo\\dao\\";
        BaseBuild base = new BaseBuild(TEMPLATE_MAPPER_DIRECTORY, ConfigReadUtil.getYmlString("generate.template.mapper"), path, "DemoMapper", FileType.XML);
        new BiGenerator(base, "utf-8", true);
    }

    @Test
    public void generator(){
        SqlLiteMapper dao = DbConnectionUtil.getSqliteDao();
        List<GeneratorConfig> configList = dao.selectGeneratorConfigAll();
        List<GeneratorConfig> tempList = new ArrayList<>();
        for (GeneratorConfig config:configList){
            GeneratorConfig temp = new Gson().fromJson(config.getData(), GeneratorConfig.class);
            tempList.add(temp.setData(null));
        }
        GeneratorConfig config = tempList.get(0);
        System.out.println(config);
        DatabaseBean databaseBean = dao.selectOneDbConnection(config.getDbInfoID());
        new MybatisCodeGenerator(config,databaseBean);//开始生成mybatis 部分代码
    }


    @Test
    public void type(){
//        String type = ConfigReadUtil.getYmlString("filed.type.convert.date","Object");
//        System.out.println(type);
//        Set<String> list = ConfigReadUtil.getYmlValueList("generate.template.dao.import");
//        System.out.println(list);
//        System.out.println("aaa".substring("aaa".lastIndexOf(".")+1));

        List<String> ymlValueList = ConfigReadUtil.getYmlStringList("generate.template.dao.import");
        System.out.println(ymlValueList);
    }

    class a{

    }


    public String read(String path){
        Yaml yaml = new Yaml();
        URL url = Thread.currentThread().getContextClassLoader().getResource("config.yml");
        String[] split = path.split("\\.");
        try {
            Map map  =(Map)yaml.load(new FileInputStream(url.getFile()));;
            for (int i=0;i<split.length;i++){
                String str = split[i];
                if(i < split.length-1)
                    map = (Map) map.get(str);
                else
                    return map.get(str)+"";
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Test
    public void dbRead(){
        String value = ConfigReadUtil.getYmlString("generate.encoding", "utf-8");
        System.out.println(value);


        String bean = ConfigReadUtil.getYmlString("generate.template.bean");
        String ctrl = ConfigReadUtil.getYmlString("generate.template.controller");
        String service = ConfigReadUtil.getYmlString("generate.template.service");
        String serviceI = ConfigReadUtil.getYmlString("generate.template.service-impl");
        System.out.println(bean+"  "+ctrl+"  "+service+" "+serviceI);
    }

    @Test
    public void readFile(){
//        URL url = Thread.currentThread().getContextClassLoader().getResource("template");
//        System.out.println(url);
        String a = "com.ss";
        System.out.println(a.replace(".", File.separator));
    }

    @Test
    public void removeInitConfig(){
        SqlLiteMapper mapper = DbConnectionUtil.getSqliteDao();
        int i = mapper.removeSystemConfig();
        LOG.info(i);

    }

    @Test
    public void testQuery(){
        DatabaseBean bean = new DatabaseBean(DbType.SqlLite,"",0,  "", "", "", "");
        //DatabaseBean bean = new DatabaseBean("MySQL","127.0.0.1",3306,  "myboot", "root", "", "utf-8");
        DbConnectionUtil.testConnection(bean,SQLLITE_DB_ID);
        SqlLiteMapper mapper = DbConnectionUtil.getSqliteDao();

        SystemConfigBean sc = mapper.selectSystemConfig();
        LOG.info(sc);
        boolean b = mapper.isExistSystemConfig();
        List<DatabaseBean> list = mapper.selectAllDbConnection();
        LOG.info(list);
    }

    @Test
    public void addConfig(){
        try {
            ComboPooledDataSource ds = new ComboPooledDataSource();
            ds.setDriverClass("com.mysql.jdbc.Driver");
            ds.setJdbcUrl("jdbc:mysql://localhost:3306/?useUnicode=true&useSSL=false&characterEncoding=utf-8");
            ds.setUser("root");
            ds.setLoginTimeout(2000);
            ds.setCheckoutTimeout(2000);
            ds.setPassword("");
            Connection actualCon = ds.getConnection();
            LOG.info(actualCon);

            Configuration config = new Configuration();
            TransactionFactory transactionFactory = new JdbcTransactionFactory();
            Environment environment = new Environment("development", transactionFactory, ds);
            config.setEnvironment(environment);
            //config.addMappers();
            SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(config);
            SqlSession session = sqlSessionFactory.openSession();
            Connection connection = session.getConnection();
            LOG.info(connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
} 
