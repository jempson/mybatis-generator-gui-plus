package com.syl.generator.common.util;

import java.io.*;
import java.util.List;

/**
 * 基础工具类
 *
 * @author syl
 * @create 2018-04-04 15:42
 **/
public class CommonUtil {

    /**
     * list 深度拷贝
     * @param src
     * @param <T> 如果是对象该对象需要序列化
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static  <T> List<T> deepCopy(List<T> src) {
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        ObjectOutputStream out = null;
        List<T> dest = null;
        try {
            out = new ObjectOutputStream(byteOut);
            out.writeObject(src);
            ByteArrayInputStream byteIn = new ByteArrayInputStream(byteOut.toByteArray());
            ObjectInputStream in = new ObjectInputStream(byteIn);
            dest = (List<T>) in.readObject();
        } catch (IOException e) {
            throw new RuntimeException("cannot deepCopy please check it pojo serialization");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return dest;
    }

}
