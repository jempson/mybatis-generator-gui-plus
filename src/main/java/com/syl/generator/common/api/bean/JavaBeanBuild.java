package com.syl.generator.common.api.bean;


import com.syl.generator.common.bean.SystemConfigBean;
import com.syl.generator.common.bean.TableColumnBean;
import com.syl.generator.common.util.CommonUtil;
import com.syl.generator.common.util.ConfigReadUtil;
import com.syl.generator.common.util.StringUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

import static com.syl.generator.common.constant.BaseConstant.CONVERT_JAVA_TYPE;
import static com.syl.generator.common.constant.BaseConstant.TEMPLATE_BEAN_DIRECTORY;

/**
 * @author syl
 * @create 2018-03-26 20:35
 **/

@Setter
@Getter
@Accessors(chain = true)
public class JavaBeanBuild extends JavaBuild{
    private List<Filed> fileds = new ArrayList<>();
    private boolean     lombok;

    /**
     * @param path  文件夹路径 d:/example/src/main/java
     * @param fileName  文件名
     * @param referencePackage 功能包  com.example.demo 注意默认会在最后带上.bean
     */
    public JavaBeanBuild(String path, String fileName,String referencePackage,SystemConfigBean systemConfigBean) {
        super(TEMPLATE_BEAN_DIRECTORY,ConfigReadUtil.getYmlString("generate.template.bean"),path, fileName,referencePackage+".bean",systemConfigBean);
        getReference();
    }

    public JavaBeanBuild setLombok(boolean lombok) {
        this.lombok = lombok;
        if(!lombok)return this;
        this.addImport("lombok.Getter");
        this.addImport("lombok.Setter");
        this.addImport("lombok.ToString");
        this.addImport("lombok.experimental.Accessors");

        this.addAnnotation("@Setter");
        this.addAnnotation("@Getter");
        this.addAnnotation("@ToString");
        this.addAnnotation("@Accessors(chain = true)");
        return this;
    }

    public JavaBeanBuild addAllFiled(List<TableColumnBean> columnList,boolean originalName) {
        int maxLength = 0;
        List<TableColumnBean> tempList = CommonUtil.deepCopy(columnList);
        for(TableColumnBean temp:tempList){
            String type = StringUtils.extract("[a-z]", temp.getColumnType().toLowerCase());
            String javaType = ConfigReadUtil.getYmlString(CONVERT_JAVA_TYPE+type+ ".java", "Object");
            if(javaType.indexOf(".") > -1)this.addImport(javaType);
            temp.setColumnType(javaType.substring(javaType.lastIndexOf(".") + 1));
            int length = javaType.substring(javaType.lastIndexOf(".")+1).length();
            if(maxLength < length)maxLength = length;
        }
        boolean textAlign = ConfigReadUtil.getYmlBoolean("generate.textalign",true);
        for(TableColumnBean bean:tempList){
            String type = bean.getColumnType();
            Filed filed = new Filed(type,bean.getColumnName(),originalName).setComment(bean.getColumnComment());
            if(textAlign)
                filed.setInterval(maxLength - type.length());
            else
                filed.setInterval(1);
            this.addFiled(filed);
        }
        return this;
    }

    public JavaBeanBuild addFiled(Filed filed){
        this.fileds.add(filed);
        return this;
    }

    @Override
    public JavaBeanBuild setRemark(String remark) {
        super.setRemark(remark);
        return this;
    }

    @Override
    public String toString() {
        return "JavaBeanBuild{" +
                "fileds=" + fileds +
                ", lombok=" + lombok +
                super.toString()+
                '}';
    }
}
