package com.syl.generator.common.api.bean;

import com.syl.generator.common.bean.SystemConfigBean;
import com.syl.generator.common.util.ConfigReadUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import static com.syl.generator.common.constant.BaseConstant.TEMPLATE_CONTROLLER_DIRECTORY;

/**
 * @author syl
 * @create 2018-03-26 20:18
 **/
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class ControllerBuild extends JavaBuild {
    private static final String CONTROLLER = "Controller";

    private String requestMapping;
    private String serviceName;
    private String beanName;
    private String voName;

    public ControllerBuild(String path, String shoreName,String referencePackage, SystemConfigBean systemConfigBean) {
        super(TEMPLATE_CONTROLLER_DIRECTORY, ConfigReadUtil.getYmlString("generate.template.controller"),
                "" , shoreName+CONTROLLER, referencePackage,systemConfigBean);
    }

}
