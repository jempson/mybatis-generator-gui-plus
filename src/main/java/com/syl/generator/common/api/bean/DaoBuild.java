package com.syl.generator.common.api.bean;

import com.syl.generator.common.bean.SystemConfigBean;
import com.syl.generator.common.bean.TableColumnBean;
import com.syl.generator.common.util.CommonUtil;
import com.syl.generator.common.util.ConfigReadUtil;
import com.syl.generator.common.util.StringUtils;
import com.syl.generator.view.bean.GeneratorConfig;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

import static com.syl.generator.common.constant.BaseConstant.QUERY_HELP_NAME;
import static com.syl.generator.common.constant.BaseConstant.TEMPLATE_DAO_DIRECTORY;

/**
 * @author syl
 * @create 2018-03-26 21:04
 *
 **/
@Accessors(chain = true)
public class DaoBuild  extends  JavaBuild{
    private static final String FILE_KEY = "generate.template.dao.";
    @Setter
    @Getter
    private List<Filed> fileds = new ArrayList<>();
    @Getter
    private String beanName;
    @Getter
    private String daoName;
    @Getter
    private String beanNameDTO;
    @Getter
    private String beanNameLower;
    @Getter
    private String queryHelpName;
    @Getter
    private String javaBeanReference;
    @Getter
    private String dtoReference;

    /**
     *
     * @param path
     * @param config 页面上的配置
     * @param systemConfig
     */
    public DaoBuild(String path, GeneratorConfig config, SystemConfigBean systemConfig) {
        super(TEMPLATE_DAO_DIRECTORY,"",path, config.getDomainObjectName()+config.getDaoName(), config.getFunctionTarget()+".dao",systemConfig);
        this.beanName = config.getDomainObjectName();
        this.daoName = config.getDomainObjectName() + config.getDaoName();
        this.beanNameLower = this.beanName.substring(0,1).toLowerCase()+this.beanName.substring(1);
        this.queryHelpName = QUERY_HELP_NAME;
        String templateType = config.isSimpleJavaBean() ? "each" : switchMode(systemConfig.getMode());
        String templateFile = ConfigReadUtil.getYmlString(FILE_KEY + templateType);
        if(StringUtils.isEmpty(templateFile))return;
        this.setTemplateFileName(templateFile);//重写模板文件路径

        if(systemConfig.getMode() == 1)
            addImport(systemConfig.getToolsPath()+"."+QUERY_HELP_NAME);
        else
            addImport(systemConfig.getToolsPath()+".*");
        List<String> importList = ConfigReadUtil.getYmlStringList("generate.template.dao.imports");
        for (String import_:importList){
            addImport(import_);
        }
        List<String> annotationList = ConfigReadUtil.getYmlStringList("generate.template.dao.annotations");
        for (String annotation:annotationList){
            addAnnotation(annotation.trim().trim());
        }
    }

    public DaoBuild addAllFiled(List<TableColumnBean> columnList,boolean originalName) {
        List<TableColumnBean> tempList = CommonUtil.deepCopy(columnList);
        for(TableColumnBean bean:tempList){
            String type = StringUtils.extract("[a-z]", bean.getColumnType().toLowerCase());
            String key = bean.getColumnKey();
            Filed filed = new Filed(type.toUpperCase(),bean.getColumnName(),originalName).setComment(bean.getColumnComment());
            filed.setPrimaryKey(!StringUtils.isEmpty(key));
            if(filed.isPrimaryKey())addFiled(filed);
        }
        return this;
    }

    public DaoBuild addFiled(Filed filed){
        this.fileds.add(filed);
        return this;
    }

    private String switchMode(int mode){
        switch (mode){
            case 0:
                return "base";
            case 1:
                return "each";
        }
        return "each";
    }

    @Override
    public DaoBuild setRemark(String remark) {
        super.setRemark(remark);
        return this;
    }

    public DaoBuild setJavaBeanReference(String javaBeanReference) {
        this.addImport(javaBeanReference);
        return this;
    }

    public DaoBuild setDtoReference(String dtoReference) {
        if(dtoReference == null){
            this.beanNameDTO = beanName;
            return this;
        }
        this.beanNameDTO = beanName+"DTO";
        this.addImport(dtoReference);
        return this;
    }

}
