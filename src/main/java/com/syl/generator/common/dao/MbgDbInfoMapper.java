package com.syl.generator.common.dao;

import com.syl.generator.common.bean.TableColumnBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 数据库信息读取
 *
 * 需按照以下格式命名
 * queryXXXTableNames(schema) 查表名
 * queryXXXColumns(schema,tableName)  查列名
 * @author syl
 * @create 2018-03-23 17:50
 **/
@Mapper
public interface MbgDbInfoMapper {

    /**
     * 查询mysql 的所有表名
     * @param schema
     * @return
     */
    List<TableColumnBean> queryMySqlTableNames(String schema);

    /**
     * 查询mysql 某个表的所有列
     * @param schema
     * @param tableName
     * @return
     */
    List<TableColumnBean> queryMySqlColumns(@Param("schema") String schema,@Param("tableName") String tableName);
}
