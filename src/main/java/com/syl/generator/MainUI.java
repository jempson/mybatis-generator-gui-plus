package com.syl.generator;

import com.syl.generator.common.bean.DatabaseBean;
import com.syl.generator.common.enums.DbType;
import com.syl.generator.common.util.DbConnectionUtil;
import com.syl.generator.common.util.SqlReservedWords;
import com.syl.generator.view.controller.MainUIController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import javax.swing.*;
import java.net.URL;
import static com.syl.generator.common.constant.BaseConstant.SQLLITE_DB_ID;

/**
 *    Copyright 2018 Zombie_Su
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 *
 * 程序主入口
 *
 * @author syl
 * @create 2018-03-17 11:11
 **/
public class MainUI  extends Application {
    private static final Logger LOG = Logger.getLogger(MainUI.class);
    
    public static Stage mainStage;

    @Override
    public void start(Stage primaryStage) throws Exception {
        SqlReservedWords.initReservedWords();//初始化数据库关键字
        mainStage = primaryStage;
        DatabaseBean bean1 = new DatabaseBean(DbType.SqlLite,"",0,  "", "", "", "");
        boolean b1 = DbConnectionUtil.testConnection(bean1,SQLLITE_DB_ID);
        LOG.info("SqlLite 连接结果："+b1);
        if(!b1){
            LOG.error("请检查resources 是否缺失config.db");
            return;
        }
        URL url = Thread.currentThread().getContextClassLoader().getResource("fxml/MainUI.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(url);
        Parent root = fxmlLoader.load();
        primaryStage.setResizable(false);
        primaryStage.setTitle("代码生成器");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

        MainUIController controller = fxmlLoader.getController();
        controller.setParent(root);

    }
    public static void main(String[] args) {
        String version = System.getProperty("java.version");
        LOG.info("java  version  "+version);
        if (Integer.parseInt(version.substring(2, 3)) >= 8 || Integer.parseInt(version.substring(0,1))>=9) {
            launch(args);
        } else {
            JFrame jFrame = new JFrame("版本错误");
            jFrame.setSize(500, 100);
            jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            JPanel jPanel = new JPanel();
            JLabel jLabel = new JLabel("JDK的版本不能低于1.8请升级至最近的JDK 1.8再运行此软件");
            jPanel.add(jLabel);
            jFrame.add(jPanel);
            jFrame.setLocationRelativeTo(null);
            jFrame.setVisible(true);

        }
    }

}
