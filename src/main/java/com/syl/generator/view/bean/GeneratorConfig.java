package com.syl.generator.view.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author syl
 * @create 2018-04-02 0:12
 **/
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class GeneratorConfig {
    /**
     * 本配置的名称
     */
    private String name;
    private String dbInfoID;
    private String data;
    /**
     * 项目所在目录
     */
    private String projectFolder;
    /**
     * 自定义接口名
     */
    private String daoName;
    /**
     * mapper包目录
     */
    private String mapperName;
    /**
     * 表名
     */
    private String tableName;
    /**
     * 表注释
     */
    private String tableComment;
    /**
     * Java实体类
     */
    private String domainObjectName;
    /**
     * 功能目标路径
     */
    private String functionTarget;
    /**
     * java文件目录
     */
    private String javaDirectory;
    /**
     * xml文件目录
     */
    private String xmlDirectory;
    /**
     * 简单javabean
     */
    private boolean simpleJavaBean;
    /**
     * 使用实际的列名
     */
    private boolean useActualColumnName;
    /**
     * 使用lombok
     */
    private boolean lombok;
    /**
     * 生成Controller
     */
    private boolean generatorController;
    /**
     * 生成Service
     */
    private boolean generatorService;
    /**
     * 生成ServiceImpl
     */
    private boolean generatorServiceImpl;
}
