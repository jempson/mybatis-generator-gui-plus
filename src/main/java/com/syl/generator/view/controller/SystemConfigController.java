package com.syl.generator.view.controller;

import com.syl.generator.common.bean.InputVerifyBean;
import com.syl.generator.common.bean.SystemConfigBean;
import com.syl.generator.common.dao.SqlLiteMapper;
import com.syl.generator.view.util.AlertUtil;
import com.syl.generator.common.util.DbConnectionUtil;
import com.syl.generator.common.util.StringUtils;
import com.syl.generator.view.util.UiBaseUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.apache.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * 系统设置控制器
 *
 * @author syl
 * @create 2018-03-17 15:25
 **/
public class SystemConfigController extends DialogBase implements Initializable {
    private static final Logger LOG = Logger.getLogger(SystemConfigController.class);

    @FXML
    private TextField nameField;
    @FXML
    private TextArea brand;
    @FXML
    private TextField toolsPath;
    @FXML
    private ComboBox generatorMode;

    public void comboBoxInit(SystemConfigBean bean){
        ObservableList<String> options = FXCollections.observableArrayList(
                "模式1",
                "模式2"
        );
        generatorMode.setItems(options);
        SingleSelectionModel model = generatorMode.getSelectionModel();
        if(bean == null) model.select(0);
        else model.select(bean.getMode());
        generatorMode.setTooltip(new Tooltip("选择生成模式"));

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        SystemConfigBean bean = DbConnectionUtil.getSqliteDao().selectSystemConfig();
        comboBoxInit(bean);//下拉框初始化
        if(bean == null)return;
        nameField.setText(bean.getName());
        brand.setText(bean.getBrand());
        String toolsPath = bean.getToolsPath();
        if(!StringUtils.isEmpty(toolsPath)) this.toolsPath.setText(toolsPath);
    }

    @FXML
    public void generatorTools(ActionEvent actionEvent) {
        InputVerifyBean bean = UiBaseUtils.inputIsEmpty(new TextInputControl[]{nameField, brand,toolsPath}, this.getParent());
        if(!bean.isPass()){
            AlertUtil.showInfoAlert(bean.getMessage()+"字段必填");
            return;
        }
        //new Boolean(toolsPath+".bean",)
    }

    @FXML
    public void saveConfig(ActionEvent actionEvent) {
        InputVerifyBean bean = UiBaseUtils.inputIsEmpty(new TextInputControl[]{nameField, brand}, this.getParent());
        if(!bean.isPass()){
            AlertUtil.showInfoAlert(bean.getMessage()+"不能为空");
            return;
        }
        int modeIndex = generatorMode.getSelectionModel().getSelectedIndex();
        SystemConfigBean scb = new SystemConfigBean()
                .setName(nameField.getText()).setBrand(brand.getText())
                .setToolsPath(toolsPath.getText()).setMode(modeIndex);
        SqlLiteMapper mapper = DbConnectionUtil.getSqliteDao();
        if(!mapper.isExistSystemConfig())
            mapper.saveSystemConfig(scb);
        else
            mapper.updateSystemConfig(scb);
        closeDialogStage();
    }

    @FXML
    public void cancel(ActionEvent actionEvent) {
        closeDialogStage();
    }

    @Override
    public void closeDialogStage() {
        if(!DbConnectionUtil.getSqliteDao().isExistSystemConfig()){
            AlertUtil.showInfoAlert("请先进行初始配置！");
            return;
        }
        super.closeDialogStage();
    }
}
