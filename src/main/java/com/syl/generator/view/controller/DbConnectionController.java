package com.syl.generator.view.controller;

import com.syl.generator.common.bean.DatabaseBean;
import com.syl.generator.common.dao.SqlLiteMapper;
import com.syl.generator.common.enums.DbType;
import com.syl.generator.view.util.AlertUtil;
import com.syl.generator.common.util.DbConnectionUtil;
import com.syl.generator.common.util.StringUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import lombok.Setter;
import org.apache.log4j.Logger;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.UUID;

/**
 * 新建数据库连接控制器
 *
 * @author syl
 * @create 2018-03-18 1:23
 **/
public class DbConnectionController extends DialogBase implements Initializable {
    private static final Logger LOG = Logger.getLogger(DbConnectionController.class);

    @Setter
    private MainUIController mainUIController;

    @FXML
    private TextField primayID;
    @FXML
    private TextField nameField;
    @FXML
    private TextField hostField;
    @FXML
    private TextField portField;
    @FXML
    private TextField userNameField;
    @FXML
    private TextField passwordField;
    @FXML
    private TextField schemaField;
    @FXML
    private ChoiceBox<String> encodingChoice;
    @FXML
    private ChoiceBox<String> dbTypeChoice;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        encodingChoice.getSelectionModel().select(0);
    }

    @FXML
    public void testConnection(ActionEvent actionEvent) {
        DatabaseBean bean = extractConfigForUI();
        if (bean == null)return;
        boolean b = DbConnectionUtil.testConnection(bean,bean.getName());
        if(!b)
            AlertUtil.showErrorAlert("数据库连接失败");
        else
            AlertUtil.showInfoAlert("连接成功");
    }

    @FXML
    public void saveConnection(ActionEvent actionEvent) {
        DatabaseBean bean = extractConfigForUI();
        if (bean == null)return;
        SqlLiteMapper dao = DbConnectionUtil.getSqliteDao();
        if(StringUtils.isEmpty(bean.getId())){//新增配置
            boolean b = dao.isExistDbConnection(bean.getName());
            if(b){
                AlertUtil.showInfoAlert("配置已经存在, 请使用其它名称");
                return;
            }
            int i = dao.saveDbConnection(bean.setId(UUID.randomUUID().toString()));
            LOG.info("新增db连接结果："+i);
        }else{
            int i = dao.updateDbConnection(bean);
            LOG.info("修改db连接结果："+i);
        }
        mainUIController.loadSchemaTreeNode();
        closeDialogStage();
    }

    public void setConfigToUI(DatabaseBean bean) {
        primayID.setText(bean.getId());
        nameField.setText(bean.getName());
        hostField.setText(bean.getHost());
        portField.setText(bean.getPort()+"");
        userNameField.setText(bean.getUsername());
        passwordField.setText(bean.getPassword());
        encodingChoice.setValue(bean.getEncoding());
        dbTypeChoice.setValue(bean.getDbType());
        schemaField.setText(bean.getSchema());
    }

    private DatabaseBean extractConfigForUI() {
        String id = primayID.getText();
        String name = nameField.getText();
        String host = hostField.getText();
        String port_ = portField.getText();
        String userName = userNameField.getText();
        String password = passwordField.getText();
        String encoding = encodingChoice.getValue();
        String dbType = dbTypeChoice.getValue();
        String schema = schemaField.getText();
        if (StringUtils.isEmptys(name, host, port_, userName, encoding, dbType, schema)) {
            AlertUtil.showWarnAlert("密码以外其他字段必填");
            return null;
        }
        int port = Integer.valueOf(port_);
        DatabaseBean bean = new DatabaseBean(DbType.valueOf(dbType),host,port,schema,userName,password,encoding);
        bean.setId(id).setName(name);
        return bean;
    }

    @FXML
    public void cancel(ActionEvent actionEvent) {closeDialogStage();}
}
