package com.syl.generator.view.controller;

import com.google.gson.Gson;
import com.syl.generator.common.dao.SqlLiteMapper;
import com.syl.generator.view.bean.GeneratorConfig;
import com.syl.generator.view.util.AlertUtil;
import com.syl.generator.common.util.DbConnectionUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import lombok.Setter;
import org.apache.log4j.Logger;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * 配置中心控制器
 *
 * @author syl
 * @create 2018-03-18 1:20
 **/
public class GeneratorConfigController  extends DialogBase implements Initializable {
    private static final Logger LOG = Logger.getLogger(GeneratorConfigController.class);

    @Setter
    private MainUIController mainUIController;
    @FXML
    private TableColumn nameColumn;
    @FXML
    private TableColumn<GeneratorConfig, String> opsColumn;
    @FXML
    private TableView<GeneratorConfig> configTable;
    private ObservableList<GeneratorConfig> configs;
    private Gson gson = new Gson();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // 自定义操作列
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        opsColumn.setCellFactory(col -> {
            TableCell<GeneratorConfig, String> cell = new TableCell<GeneratorConfig, String>() {
                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    this.setText(null);
                    this.setGraphic(null);
                    if (!empty){
                        Button btn1 = new Button("应用");
                        Button btn2 = new Button("删除");
                        HBox hBox = new HBox();
                        hBox.setSpacing(10);
                        hBox.getChildren().add(btn1);
                        hBox.getChildren().add(btn2);
                        btn1.setOnAction(event -> {
                            // 应用配置
                            GeneratorConfig config = configs.get(this.getIndex());
                            closeDialogStage();
                            mainUIController.setGeneratorConfigIntoUI(config);
                        });
                        btn2.setOnAction(event -> {
                            // 删除配置
                            GeneratorConfig config = configs.get(this.getIndex());
                            int i = DbConnectionUtil.getSqliteDao().removeGeneratorConfig(config.getName());
                            LOG.info("删除配置:"+i+" name: "+config.getName());
                            refreshTableView();
                        });
                        this.setGraphic(hBox);
                    }
                }
            };
            return cell;
        });

        refreshTableView();
    }

    /**
     * 刷新列表数据
     */
    public void refreshTableView() {
        try {
            SqlLiteMapper dao = DbConnectionUtil.getSqliteDao();
            List<GeneratorConfig> configList = dao.selectGeneratorConfigAll();//存入sqllite的配置为name data格式的数据
            List<GeneratorConfig> tempList = new ArrayList<>();
            for (GeneratorConfig config:configList){
                GeneratorConfig temp = gson.fromJson(config.getData(), GeneratorConfig.class);//把data数据转换为原对象
                tempList.add(temp.setData(null));
            }
            configs = FXCollections.observableList(tempList);
            configTable.setItems(configs);
        } catch (Exception e) {
            AlertUtil.showErrorAlert(e.getMessage());
        }
    }

}

