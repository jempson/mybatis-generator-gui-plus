# mybatis-generator-gui-plus
这是mybatis的一个增强生成器，界面操作方便快捷\
有兴趣可以加 Q群745697373

mybatis-generator-gui-plus基于[mybatis-generator-gui](https://github.com/zouzg/mybatis-generator-gui)的ui界面开发。不依赖[mybatis-generator](http://www.mybatis.org/generator/running/running.html)带来的脱胎换骨

## 为什么要使用它?
    相信一些朋友都使用过mybatis-generator(以下简称MBG)
    使用过程或多或少都遇到一些问题，抛开配置不说，它的很多功能可能不是你想要的
    你查阅了很久资料修改了部分代码能满足你的需求了(排版格式化,表注释,列注释)
    这时候你会写自己的MBG插件了
    
## 好像已经皆大欢喜了是吗?
    可能对有部分人是够了
    但是喜欢折腾的朋友会发现如下问题生成的dao接口不够满意(比如我想增减接口)
    好我们加个MBG插件，但发现事情没这么简单
    由于MBG核心生成代码都是在代码中调用的不同方法，那么你必须侵入代码增减代码。
    翻山越岭并穿过原始深林后终于到达你的目的地 这时候你不禁要问
    为什么这么麻烦！

## 那本工具呢
- 使用freemarker让生成代码非常简单 (如果你熟悉模板语言那么修改起来就非常方便)
- 抛开界面可任意生成代码文件（如果你要写界面当然也可以）
- 扩展性强
- 数据库支持MySql

######  只支持一个数据库有没有搞错啊?
    因为当前版本并不是完全体,数据库扩展有非常方便的方式
    在后续的迭代中会支持更多常用数据库

### 使用必看 请认真阅读
github地址 [mybatis-generator-gui-plus](https://gitee.com/zombie1993/mybatis-generator-gui-plus)
1. jdk版本必须为1.8以上
2. 本项目使用了lombok 如ide没有lombok插件将运行失败请按如下传送门指示
 [eclipse](https://blog.csdn.net/icecoola_/article/details/77414572)\
 [idea](https://blog.csdn.net/zhglance/article/details/54931430)
3. 如果数据库为oracle 请确保引用了ojdbc14.jar 因为本项目使用maven构建，oracle的驱动包因版权在maven
上找不到最新的。（百度的 如果有其他途径请在issue提出来）
4. 该项目还在迭代当中,目前只适用于mysql (请忽略3)
5. 如果大家使用没有太大问题会尽快更新其他数据库
6. 使用该工具项目需要引用[crud-common](https://gitee.com/zombie1993/crud-common) 它是需要依赖的工具类
```
你可以使用maven进行依赖 也可以打成jar后引入
重要说明： 不是在本项目！是在你生成代码的那项目
<dependency>
    <groupId>com.syl.framework</groupId>
    <artifactId>crud-common</artifactId>
    <version>1.0</version>
</dependency>
```
7. 如果使用中遇到什么bug 请在[issue](https://gitee.com/zombie1993/mybatis-generator-gui-plus/issues)提出来
---

### 使用说明
    1. 下载后在ide中运行 入口类com.syl.generator.MainUI 
    2. 本工具生成按功能进行划分
    例: 功能目标路径：com.xxx.demo.user
    com
       xxx
          demo
              user #目标功能 生成的功能都会在这个包下
                bean
                dao
                ...
    3. xml资源如果不写默认生成在和dao目录同级
    4. 除了dao和不带Generator的mapper的文件,其他文件不建议在生成后修改代码
    因为他们是覆盖生成的，如果要改变这一情况也很容易 关键代码MybatisCodeGenerator.startGenerator()
    中把BiGenerator参数修改为false就不会覆盖
    5. 表注释一定要写！表注释一定要写！表注释一定要写！
    6. 表可多选,按shift+右键
### javaben 说明
    本工具默认生成的bean对象有两个 如果后续更新controller后还会增加1个
    如果不希望生成请选择简单javaBean属性
    
    TableName 最简单的对象
    和数据库字段保持一致 不负责任何复杂场景,使用于service接口返回参数
    DTO db数据传输对象
    相较于普通bean 对象复杂,负责和sql真实交互的对象,当类型为Date时附加DateStart 和DateEnd 属性(可能还会有其他特殊的类型)
    使用于service接口参数
    VO  view数据传输对象
    可能和数据库字段不一样,负责和页面交互时使用的数据层,相较DTO更为复杂,
    会使用@Validated注解对字段进行后台二次验证,字段格式化等等操作,使用于controller 返回页面和接口参数上



 - - -
 Copyright 2018 Zombie_Su\
 Licensed under the Apache License, Version 2.0 (the "License");\
 you may not use this file except in compliance with the License.\
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.